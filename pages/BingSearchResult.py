from selenium.webdriver.common.by import By


class BingSearchResult:
    @staticmethod
    def get_links(driver):
        return driver.find_elements(By.XPATH, '//div[@class = "b_title"]/h2/a')

    @staticmethod
    def get_names(driver):
        return driver.find_elements(By.XPATH, '//div[@class = "b_title"]/h2/a/span')

    @staticmethod
    def check_links(link):
        if str(link).startswith('https://www.expedia.com') and str(link).endswith('Hotel-Information'):
            return True



