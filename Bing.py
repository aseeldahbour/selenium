
import allure
import unittest
from ddt import ddt, data, unpack
from Read import Read
import time
from selenium import webdriver

from Write import Write
from pages.BingSearchPage import BingSearchPage
from pages.BingSearchResult import BingSearchResult


@ddt
class Bing(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.file_info = []
        cls.headers = ["hotel_id", "name", "city", "address"]
        cls.driver = webdriver.Chrome('web_driver/chromedriver.exe')
        time.sleep(20)
        print("setup class")

    @data(*Read.read_file("csv_file/input_(1).csv"))
    @unpack
    def test_look_for_bing_page(self, hotel_id, name, city, address):
        with allure.step("look for bing page"):
            final_result_dict = {}
            final_result_dict.update({"hotel_id": hotel_id, "name": name, "city": city, "address": address})
            bing_page = BingSearchPage()
            bing_page.open_bing_page(self.driver)
            bing_page.search(self.driver, name + " " + city + " expedia")
            elements = BingSearchResult()
            links = elements.get_links(self.driver)
            if len(links) > 0:
                i = 1
                for link in links:
                    link_text = link.get_attribute("href")
                    link_result = elements.check_links(link_text)
                    if link_result:
                        name_text = link.text
                        final_result_dict.update({"found hotel name" + str(i): name_text, "found hotel link" + str(i): link_text})
                        if "found hotel name" + str(i) not in self.headers:
                            self.headers.append("found hotel name" + str(i))
                        if "found hotel link" + str(i) not in self.headers:
                            self.headers.append("found hotel link" + str(i))

                        i += 1
                if i == 1:
                    final_result_dict.update({"hotel_name1": "Name not Found", "hotel_link1": "hotel Not Found"})

            else:
                final_result_dict.update({"hotel_name1": "Name not Found", "hotel_link1": "hotel Not Found"})

            self.file_info.append(final_result_dict)

    @classmethod
    def tearDownClass(cls):
        Write.write_file("csv_file/output.csv", cls.file_info, cls.headers)
        cls.driver.close()
        print("tear down")
