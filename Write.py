import csv


class Write:
    @staticmethod
    def write_file(filename, data, headers):
        path = "./" + filename
        with open(path, 'w', newline='') as csvfile:
            csv_writer = csv.writer(csvfile)
            csv_writer.writerow(headers)
            for row in data:
                csv_writer.writerow(row.values())
